{ pkgs, ... }:

{
  languages.python = {
    enable = true;
    version = "3.9";
    venv.enable = true;
    venv.requirements = ./requirements.txt;
  };

  # https://devenv.sh/basics/
  env.GREET = "devenv";

  # https://devenv.sh/packages/
  packages = [
    pkgs.git
  ];

  # https://devenv.sh/scripts/
  scripts.hello.exec = "echo hello from $GREET";

  processes.somepyapp.exec = "python -m somepyapp.somepyapp";

  #containers."somepyapp".name = "somepyapp";
  #containers."somepyapp".startupCommand = processes.somepyapp.exec;

  enterShell = ''
    hello
    git --version
  '';

  # https://devenv.sh/languages/
  # languages.nix.enable = true;

  # https://devenv.sh/pre-commit-hooks/
  # pre-commit.hooks.shellcheck.enable = true;

  # https://devenv.sh/processes/
  # processes.ping.exec = "ping example.com";

  # See full reference at https://devenv.sh/reference/options/
}
